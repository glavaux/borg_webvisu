import pandas as pd
from astroquery.ned import Ned, conf
from astropy import units as u
from astropy.coordinates import SkyCoord

conf.correct_redshift = "1"

df = pd.DataFrame(columns=["Name", "X", "Y", "Z"])

for name in [
    "Virgo cluster",
    "Coma cluster",
    "Perseus cluster",
    "Pisces cluster",
    "Norma cluster",
    "Hydra cluster",
    "Centaurus cluster",
    "NGC 4993",
    "Hercules cluster",
    "Ophiuchus cluster"
]:
    r = Ned.query_object(name)
    c = SkyCoord(
        ra=r[0]["RA"] * u.degree,
        dec=r[0]["DEC"] * u.degree,
        distance=r[0]["Velocity"] / 100.0,
        frame="icrs",
    )
    x, y, z = c.cartesian.xyz
    df = df.append({"Name": name, "X": x, "Y": y, "Z": z}, ignore_index=True)

df.to_csv("clusters.csv", index=False)
