import dash
import plotly.express as px
import pandas as pd
import plotly.graph_objects as go
import numpy as np
import h5py as h5
from dash_extensions.enrich import (
    Output,
    Input,
    Serverside,
    Dash,
)

na = np.newaxis

clusters = pd.read_csv("clusters.csv")
L = 677.7
Ng = 256
k_axis = np.fft.fftfreq(Ng, d=L / Ng) * 2 * np.pi
k_norm = np.sqrt(
    k_axis[:, na, na] ** 2
    + k_axis[na, :, na] ** 2
    + k_axis[na, na, : (Ng // 2 + 1)] ** 2
)

s_base = 20


def build_mesh(pos, voxel_step):
    s = s_base * voxel_step
    voxel_size = L / Ng * voxel_step / 2
    Nreduced = Ng // voxel_step
    X, Y, Z = np.mgrid[0:Nreduced, 0:Nreduced, 0:Nreduced] * L / Nreduced - L / 2

    slicer = slice((Ng // 2 - s + pos) // voxel_step, (Ng // 2 + s + pos) // voxel_step)
    slicer = (slicer, slicer, slicer)
    lX = X[slicer]
    lY = Y[slicer]
    lZ = Z[slicer]
    return lX, lY, lZ


def build_data(mcmc, pos, voxel_step):
    voxel_size = L / Ng * voxel_step / 2
    s = s_base * voxel_step
    try:
        with h5.File(f"data/mcmc_{mcmc}.h5", mode="r") as ff:
            rho = np.log(2 + ff["scalars/BORG_final_density"][:])
            rho_hat = np.fft.rfftn(rho)
            rho_hat *= np.exp(-0.5 * k_norm ** 2 * voxel_size ** 2)
            rho = np.fft.irfftn(rho_hat)[::voxel_step, ::voxel_step, ::voxel_step]
    except:
        return build_data(8700, pos)

    complete_slicer = ()
    for p in pos:
        complete_slicer = complete_slicer + (
            slice((Ng // 2 - s + p) // voxel_step, (Ng // 2 + s + p) // voxel_step),
        )
    rho = rho[complete_slicer]

    data = rho

    return pd.DataFrame(
        {"rho": data.flatten()},
        index=pd.MultiIndex.from_product(
            [
                range(2 * s // voxel_step),
                range(2 * s // voxel_step),
                range(2 * s // voxel_step),
            ]
        ),
    )


def setup_visu(app: Dash):
    @app.callback(
        Output("mesh", "data"),
        [Input("voxel-step", "value")],
        memoize=True,
    )
    def update_mesh(voxel_step):
        lX, lY, lZ = build_mesh(0, voxel_step)
        return Serverside(pd.DataFrame({"X": lX.flatten(), "Y": lY.flatten(), "Z": lZ.flatten()}))

    @app.callback(
        Output("store", "data"),
        [
            Input("mcmc-slider", "value"),
            Input("x-slider", "value"),
            Input("y-slider", "value"),
            Input("z-slider", "value"),
            Input("voxel-step", "value"),
        ],
        memoize=True,
    )
    def update_data(selected_mcmc, x, y, z, voxel_step):
        return Serverside(build_data(selected_mcmc, [x, y, z], voxel_step))

    @app.callback(
        Output("visible-clusters", "data"),
        [
            Input("x-slider", "value"),
            Input("y-slider", "value"),
            Input("z-slider", "value"),
        ],
        memoize=True,
    )
    def update_clusters(x, y, z):
        local_clusters = clusters.copy()
        local_clusters["X"] -= x * L / Ng
        local_clusters["Y"] -= y * L / Ng
        local_clusters["Z"] -= z * L / Ng
        return Serverside(local_clusters)

    @app.callback(
        Output("graph-with-slider", "figure"),
        [
            Input("visible-data", "value"),
            Input("visible-clusters", "data"),
            Input("store", "data"),
            Input("mesh", "data"),
        ],
        memoize=True,
    )
    def update_figure(visible, clusters_data, fig_data, this_mesh):
        lX, lY, lZ = this_mesh["X"], this_mesh["Y"], this_mesh["Z"]
        data = []
        #    clusters_data = pd.read_json(clusters_data)

        if "CLU" in visible:
            data.append(
                go.Scatter3d(
                    x=clusters_data["X"],
                    y=clusters_data["Y"],
                    z=clusters_data["Z"],
                    mode="markers",
                    hovertext=clusters_data["Name"],
                )
            )

        if "ISO" in visible:
            data.append(
                go.Isosurface(
                    name="Universe",
                    x=lX.values,
                    y=lY.values,
                    z=lZ.values,
                    value=fig_data["rho"].values,
                    isomin=1.0,
                    isomax=3,
                    opacity=0.5,  # needs to be small to see through all surfaces
                    # needs to be a large number for good volume rendering
                    # colorscale='RdBu_r',
                    surface_count=3,
                    caps=dict(x_show=False, y_show=False),
                )
            )

        return {
            "data": data,
            "layout": {"uirevision": "Universe",},
        }
