FROM docker.io/library/python:3.9 AS build

RUN mkdir /app /app/data
COPY requirements.txt /app
RUN pip3 install --no-cache-dir -r /app/requirements.txt
WORKDIR /app

FROM build
COPY --from=build /app /app

COPY *.py clusters.csv /app

ENTRYPOINT ["/usr/local/bin/gunicorn"]
VOLUME /app/data

CMD ["app:server", "-b", ":8181"]
