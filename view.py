import dash
from dash import dcc
import dash_bootstrap_components as dbc
from dash import html


def setup_view(fig):

    settings = dbc.Form(
        [
            dbc.Row(
                [
                    dbc.Label("MCMC identifier"),
                    dbc.Col(dcc.Slider(
                        id="mcmc-slider",
                        min=8100,
                        max=9600,
                        value=8700,
                        marks={str(i): str(i) for i in range(8100, 9600, 500)},
                        step=100,
                    )),
                ]
            ),
            dbc.Row(
                [
                    dbc.Label("Shift the cube center (X axis)"),
                    dbc.Col(dcc.Slider(id="x-slider", min=-50, max=50, value=0, step=10)),
                ]
            ),
            dbc.Row(
                [
                    dbc.Label("Shift the cube center (Y axis)"),
                    dbc.Col(dcc.Slider(id="y-slider", min=-50, max=50, value=0, step=10)),
                ]
            ),
            dbc.Row(
                [
                    dbc.Label("Shift the cube center (Z axis)"),
                    dbc.Col(dcc.Slider(id="z-slider", min=-50, max=50, value=0, step=10)),
                ]
            ),
            dbc.Row(
                [
                    dbc.Label("Resolution / volume"),
                    dbc.Col(dcc.Slider(id="voxel-step", min=1, max=4, value=1, step=1)),
                ]
            ),
            dcc.Checklist(
                id="visible-data",
                options=[
                    {"label": "Density", "value": "ISO"},
                    {"label": "Clusters", "value": "CLU"},
                ],
                value=["ISO", "CLU"],
            ),
        ]
    )

    return dbc.Container(
        children=[
            html.H3("BORG-PM 2M++ visualization"),
            html.P(
                "Here you can explore interactively some results obtained from the BORG-PM algorithm on the 2M++ data."
            ),
            html.Hr(),
            dbc.Row(
                children=[
                    dbc.Col(dbc.Container(settings, fluid=True), width=3),
                    dbc.Col(
                        children=[
                            dcc.Loading(
                                children=[
                                    dcc.Store(id="store"),
                                    dcc.Store(id="visible-clusters"),
                                    dcc.Store(id="mesh"),
                                ],
                                fullscreen=True,
                                type="dot",
                            ),
                            dcc.Graph(id="graph-with-slider", figure=fig),
                        ],
                        width=9,
                    ),
                ]
            ),
        ],
        fluid=True,
    )
