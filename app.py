# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
from dash import dcc
import dash_bootstrap_components as dbc
from dash import html
import plotly.express as px
import pandas as pd
import plotly.graph_objects as go
import numpy as np
import flask
from flask_caching import Cache
import h5py as h5

##from dash.dependencies import Input, Output
from dash_extensions.enrich import (
    DashProxy,
    ServersideOutputTransform,
    FileSystemBackend,
    Dash,
)
import os
from view import setup_view
from visu import setup_visu

BASE_PATH = os.getenv("DASH_BASE_PATHNAME", "/")


external_stylesheets = [dbc.themes.BOOTSTRAP]

server = flask.Flask(__name__)
fss = FileSystemBackend("_cache", threshold=200,default_timeout=300)
app = DashProxy(
    __name__,
    transforms=[ServersideOutputTransform(backends=[fss])],
    external_stylesheets=external_stylesheets,
    server=server,
    url_base_pathname=BASE_PATH,
)


cache = Cache(
    app.server,
    config={
        # try 'filesystem' if you don't want to setup redis
        "CACHE_TYPE": "filesystem",
        "CACHE_DIR": "_cache",
        #'CACHE_REDIS_URL': os.environ.get('REDIS_URL', '')
    },
)

fig = go.Figure(layout={"height": 600},)
fig.update_layout(
    scene={
        "xaxis": dict(nticks=6, range=[-40.0, 40.0]),
        "yaxis": dict(nticks=6, range=[-40.0, 40.0]),
        "zaxis": dict(nticks=6, range=[-40.0, 40.0]),
        "xaxis_title": "Equatorial X",
        "yaxis_title": "Equatorial Y",
        "zaxis_title": "Equatorial Z",
    },
    scene_aspectmode="cube",
)

app.layout = setup_view(fig)

setup_visu(app)

if __name__ == "__main__":
    app.run_server(debug=True)
